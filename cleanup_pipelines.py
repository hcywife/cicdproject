import requests
import os

# GitLab API相关信息
gitlab_url = "https://gitlab.com/hcywife/cicdproject"  # 替换为你的GitLab实例URL
project_id = cicdproject  # 替换为你的项目ID
access_token = "glpat-xkwcJHm8spSkyx5hbnGP"  # 替换为你的访问令牌

# 获取当前提交的Pipeline ID
current_pipeline_id = os.getenv("CI_PIPELINE_ID")

# 获取提交者的用户名
author_username = os.getenv("CI_COMMIT_USERNAME")

# 获取提交者的所有Pipeline
headers = {"Authorization": f"Bearer {access_token}"}
response = requests.get(f"{gitlab_url}/api/v4/projects/{project_id}/pipelines?username={author_username}", headers=headers)
pipelines = response.json()

# 中断旧的Pipeline
for pipeline in pipelines:
    pipeline_id = pipeline["id"]
    if pipeline_id != current_pipeline_id:
        response = requests.post(f"{gitlab_url}/api/v4/projects/{project_id}/pipelines/{pipeline_id}/cancel", headers=headers)
        if response.status_code == 202:
            print(f"Canceled pipeline: {pipeline_id}")
        else:
            print(f"Failed to cancel pipeline: {pipeline_id}")

# 中断除最新Pipeline之外的所有Pipeline
latest_pipeline_id = pipelines[0]["id"]
for pipeline in pipelines[1:]:
    pipeline_id = pipeline["id"]
    response = requests.post(f"{gitlab_url}/api/v4/projects/{project_id}/pipelines/{pipeline_id}/cancel", headers=headers)
    if response.status_code == 202:
        print(f"Canceled pipeline: {pipeline_id}")
    else:
        print(f"Failed to cancel pipeline: {pipeline_id}")
