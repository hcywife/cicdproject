import requests
import os

# glpat-xkwcJHm8spSkyx5hbnGP ling
# glpat-Kh6PKASfJhqUkpvZ5_HH owner
# glpat-u8TUHw9bZA8pCAnu-Bif maintianer
# curl --header "PRIVATE-TOKEN: glpat-Kh6PKASfJhqUkpvZ5_HH" --request "DELETE" "https://gitlab.com/api/v4/projects/51483271/pipelines/1054423445"
# curl --header "PRIVATE-TOKEN: glpat-Kh6PKASfJhqUkpvZ5_HH" "https://gitlab.com/api/v4/projects/51483271/pipelines"
# curl --request POST --header "PRIVATE-TOKEN: glpat-u8TUHw9bZA8pCAnu-Bif" "https://gitlab.com/api/v4/projects/51483271/pipeline?ref=main"
# curl --header "PRIVATE-TOKEN: glpat-Kh6PKASfJhqUkpvZ5_HH" "https://gitlab.com/api/v4/projects/51483271/pipelines/1057146138"


headers = {'PRIVATE-TOKEN': 'glpat-Kh6PKASfJhqUkpvZ5_HH'}
url = "https://gitlab.com/api/v4/projects/51483271/pipelines"
keep_num = 1

##获取commiter
response = requests.get(url,headers=headers).json()
id = []
for i in response:
    id.append(i['id'])
url_user = f"https://gitlab.com/api/v4/projects/51483271/pipelines/{id[0]}"
response_user = requests.get(url_user,headers=headers).json()
commiter = response_user['user']['username']
##获取commiter所有pipeline
url_user = f"https://gitlab.com/api/v4/projects/51483271/pipelines?username={commiter}"
response_user = requests.get(url_user, headers=headers).json()
id = []
for i in response_user:
    id.append(i['id'])
##delete
id = id[keep_num:]
for i in id:
    url_user = f"https://gitlab.com/api/v4/projects/51483271/pipelines/{i}"
    response_user = requests.get(url_user,headers=headers).json()
    if response_user['user']['username'] == commiter:
        url = f"https://gitlab.com/api/v4/projects/51483271/pipelines/{i}/cancel"
        response = requests.post(url, headers=headers)
