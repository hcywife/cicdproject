#!/bin/bash

# 获取最新的 Commit SHA
LATEST_COMMIT=$(git rev-parse HEAD)

# 获取最新的 Commiter
LATEST_COMMITER=$(git log -1 --pretty=format:'%an')

# 获取旧的 Pipeline ID
OLD_PIPELINE_ID=$(curl --header "PRIVATE-TOKEN: glpat-xkwcJHm8spSkyx5hbnGP" "https://gitlab.example.com/api/v4/projects/51483271/pipelines?ref=master&username=$LATEST_COMMITER" | jq -r '.[].id')

# 停止旧的 Pipeline
for PIPELINE_ID in $OLD_PIPELINE_ID; do
  if [ "$PIPELINE_ID" != "$CI_PIPELINE_ID" ]; then
    curl --request POST --header "PRIVATE-TOKEN: glpat-xkwcJHm8spSkyx5hbnGP" "https://gitlab.example.com/api/v4/projects/51483271/pipelines/$PIPELINE_ID/cancel"
  fi
done
